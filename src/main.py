from trainerbase.main import run

from gui import run_menu
from injections import update_player_coord_struct_pointer


def on_initialized():
    update_player_coord_struct_pointer.inject()


if __name__ == "__main__":
    run(run_menu, on_initialized)
