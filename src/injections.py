from trainerbase.codeinjection import AllocatingCodeInjection
from trainerbase.memory import pm

from memory import player_coord_struct_pointer


update_player_coord_struct_pointer = AllocatingCodeInjection(
    pm.base_address + 0x21B537,
    f"""
        mov [{player_coord_struct_pointer}], ecx

        mov ecx, [ecx + 0x24]
        mov [esi + 0x14], ecx
    """,
    original_code_length=6,
)

infinite_ammo = AllocatingCodeInjection(
    pm.base_address + 0x1400CC,
    """
        mov dword [eax + 0x294], 999

        mov eax, [eax + 0x294]
    """,
    original_code_length=6,
)

infinite_health = AllocatingCodeInjection(
    pm.base_address + 0x1A2A5,
    f"""
        mov dword [{pm.base_address + 0x34C5A4}], 9999
        mov dword [ecx + 0xE0], 9999

        cmp [ecx + 0xE0], esi
    """,
    original_code_length=6,
)

one_hit_kill = AllocatingCodeInjection(
    pm.base_address + 0x84B20,
    """
        mov dword [esi + 0xE0], 0

        sub [esi + 0xE0], ecx
    """,
    original_code_length=6,
)
