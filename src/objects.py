from trainerbase.gameobject import GameInt
from trainerbase.memory import Address

from memory import player_coord_struct_pointer


player_coord_struct_address = Address(player_coord_struct_pointer, [0x24])
player_x = GameInt(player_coord_struct_address)
player_y = GameInt(player_coord_struct_address + 0x4)
player_z = GameInt(player_coord_struct_address + 0x8)
