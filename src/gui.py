from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.misc import SeparatorUI
from trainerbase.gui.teleport import TeleportUI

from injections import infinite_ammo, infinite_health, one_hit_kill
from teleport import tp


@simple_trainerbase_menu("ZDoom", 600, 300)
def run_menu():
    add_components(
        CodeInjectionUI(infinite_health, "Infinite Health", "J"),
        CodeInjectionUI(infinite_ammo, "Infinite Ammo", "K"),
        CodeInjectionUI(one_hit_kill, "One Hit Kill", "L"),
        SeparatorUI(),
        TeleportUI(tp, "I", "O", "P"),
    )
