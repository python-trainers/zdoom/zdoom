from typing import override

from trainerbase.common.teleport import Number, Teleport, Vector3

from objects import player_x, player_y, player_z


class ZDoomTeleport(Teleport):
    @override
    def set_coords(self, x: Number, y: Number, z: Number = 100) -> None:
        return super().set_coords(int(x), int(y), int(z))


tp = ZDoomTeleport(
    player_x,
    player_y,
    player_z,
    dash_coefficients=Vector3(10_000_000, 10_000_000, 10_000_000),
    minimal_movement_vector_length=1_900_000,
)
